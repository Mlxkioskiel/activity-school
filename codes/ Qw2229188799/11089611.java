/**
 * 冒泡 排序 函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for (int i = 0; i < n; i++) {
    	for (int j = 0; j < i; j++) {
		if (a[i] <  a[j]) {
		 	int t = a[i];
			a[i] = a[j];
			a[j] = t;
		}
	}
    }
}//end
