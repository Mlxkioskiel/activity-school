public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标记是否发生交换，用于优化，如果这一趟没有发生交换，说明已经有序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记发生了交换  
                swapped = true;  
            }  
        }  
        // 如果这一趟没有发生交换，说明数组已经有序，提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
