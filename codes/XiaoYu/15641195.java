/**  
 * 冒泡排序函数  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标志位，表示这一趟是否有交换，优化冒泡排序的关键  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，将标志位设为true  
                swapped = true;  
            }  
        }  
        // 如果这一趟没有发生交换，说明数组已经有序，直接退出循环  
        if (!swapped) {  
            break;  
        }  
    }  
}
