/**  
     * 冒泡排序函数  
     * @param a 待排序的数组  
     * @param n 待排序的数组长度  
     */  
    public static void bubbleSort(int[] a, int n) {  
        if (n <= 1) {  
            return; // 如果数组长度为0或1，则不需要排序  
        }  
  
        for (int i = 0; i < n - 1; i++) { // 外层循环控制遍历次数  
            for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每次比较的元素  
                if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们  
                    int temp = a[j];  
                    a[j] = a[j + 1];  
                    a[j + 1] = temp;  
                }  
            }  
        }  
    } 
