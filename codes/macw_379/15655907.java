/**  
 * 冒泡排序函数  
 * 依次比较相邻的元素，如果它们的顺序错误就把它们交换过来  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制排序趟数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，控制每一趟排序多少次  
            // 如果前一个元素大于后一个元素，则交换位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j]; // 使用临时变量进行交换  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
