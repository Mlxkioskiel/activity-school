/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    int i, j, tmp_data;

    for(i = 0; i < n - 1; i ++) {
        for(j = 0; j < n - 1 - i; j ++) {
            if(a[j] > a[j + 1]) {
                tmp_data =  a[j + 1];
                a[j + 1] = a[j];
                a[j] = tmp_data;
            }
        }
    }
} //end
