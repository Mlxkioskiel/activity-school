/**  
 * 冒泡排序函数  
 * 将数组a按照升序进行排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序的轮数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环进行相邻元素的比较和交换  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素  
                // 交换两个元素的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end  
