/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较和交换，将较大的元素逐步“浮”到数组的末尾，实现排序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    // 外层循环控制排序的轮数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环控制每轮排序的比较次数  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
    // 当所有轮排序完成后，数组 a 变得有序  
} // end
