/**  
 * 冒泡排序函数  
 * 对数组a进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 用于标记本轮是否有交换，优化冒泡排序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记本轮发生了交换  
                swapped = true;  
            }  
        }  
        // 如果本轮没有发生交换，则数组已经有序，可以提前退出循环  
        if (!swapped) {  
            break;  
        }  
    }  
} //end   
