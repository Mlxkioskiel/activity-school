/**  
 * 冒泡排序函数  
 * 遍历数组，比较相邻元素，如果顺序错误则交换它们，直到没有元素需要交换，数组排序完成。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 设置一个标志位，表示这一趟是否有交换  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 如果有交换发生，则将标志位设为 true  
                swapped = true;  
            }  
        }  
        // 如果这一趟没有发生交换，说明数组已经有序，直接退出循环  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
